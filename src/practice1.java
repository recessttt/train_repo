import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class practice1 {
	
	public static void main(String[] args) {
		
//	1月6日研修
/*	lengthとlength()の違い
 * 	lrngth 　配列
 * 	length()文字列
 */
	String name = "Taiki Wakabayashi";
	int[] Number = {22,34,57,78,2354,7};
		
	System.out.println(name.length());
	System.out.println(Number.length);
	
	
/*	parseInt()とvalueOf()の違い
 * 	parseInt()　⇨　戻り値がint型
 * 	valueof()　⇨　戻り値がラッパークラス（Integer）
 */	
	Integer i = Integer.parseInt("99");
	int o = Integer.parseInt("99");
	Integer q = Integer.valueOf("99");
	int s = Integer.valueOf("99");
	System.out.println(i + o + q + s);
	
/*1月7日研修で学んだ内容
 * ArrayList　について
 */
	ArrayList<String> str = new ArrayList<>();
	str.add("taiki");	
	str.add("misaki");
	//コレクションの要素数を確認する時はlengthではなく、size()
	str.size();
	//コレクションはプリミティブ型は格納できないので、ラッパークラスにする必要あり。
	ArrayList<Integer> inte = new ArrayList<>();
	inte.add(5);
	
/*1月11日研修で学んだ内容
* 
*/
	//double型は64bit　int型は32bit（doubleの方が大きい）
	int i1 = 3;
	int i2 = 5;
	double d1 = i1 + i2;
	System.out.println(d1);
	
	//concatは、文字列を繋げた後new String()で新しいインスタンスを返している。
	//concatメソッドを利用したら参照値を再代入しないと、連結した文字が出力されない。
	String strg = "Java";
	strg.concat("Rules!");
	System.out.println(strg);
	strg = strg.concat("World!");
	System.out.println(strg);
	
	//1月12日に学んだ事
	String val = "A";
//①ラムダ式のスコープはラムダ式を囲っているものと同じスコープなので、同じ名前の変数を定義する事はできない。
//	Function f = (val)->{
//		System.out.println(val);
//	};
	Function f = (v)->{
		System.out.println(v);
	};
	f.test(val);
	
	List<String> list1 = new ArrayList<>(Arrays.asList(new String[] {"A","B","C"}));
	List<String> list2 = Arrays.asList(new String[] {"A","B","C"});
	
	list1.removeIf(
			(String chara)->{
				return chara.equals("B");
						}
			);
	System.out.println(list1);
	
	//asListメソッドで生成しているので、removeIfで消すと例外がスローする
	list2.removeIf(
			(String chara)->{
				return chara.equals("B");
						}
			);
	System.out.println(list2);
	}
	
	//1月12日①のメソッド
	interface Function{
		void test(String val);
	}
}
